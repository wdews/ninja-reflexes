<div class="pad-left">
<div class="logo">
<h1>Ninja<br />Reflexes</h1>
<img id="logo-image" src="images/ninja.png" alt="Ninja Character" />
<div class="clear">&nbsp;</div>
</div>
<article id="before">
<p>Well, here you have it...the cream of the crop, the elite of the elite. If you're on this list, give yourself
a pat on the back, and tell your friends. You Ninja, you :)</p>
<p style="text-align: center;"><a class="button-small" href="/">&laquo; Go Back</a>
<p>Based on findings at
<a href="http://www.humanbenchmark.com/tests/reactiontime/stats.php" title="Open in new window" target="_blank">HumanBenchmark.com</a>,
the average human reaction time is 215ms. Ninjas? They think that's slow. Slow = Bad.</p>
</article>
</div>