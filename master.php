<?php require_once('core/functions.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Ninja Reflexes // <?php echo $page_title; ?></title>
<?php require_once('core/references.php'); ?>
<link rel="image_src" href="/images/ninja.png" />
<!--[if lt IE 9]>
<script src="scripts/html5shiv.js"></script>
<![endif]-->
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=411324292222400";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="allwrapper">
<div class="wrapper">
<section id="main">
<?php include($initial_content); ?>
</section>
<aside id="test">
<?php include($page_content); ?>
</aside>
<div class="clear">&nbsp;</div>
</div><!-- #wrapper -->
<div class="clear">&nbsp;</div>
<div id="about" class="fancybox-contents" style="width:400px;">
I'm a simple guy who earns his living building websites, and enjoys long walks on the beach,
luxurious bubble baths, and dalmation puppies. All joking aside, my name is Wes. I made Ninja
Reflexes because...I randomly felt the urge to build something. I hope you enjoy it!
</div>
<div id="scores" class="fancybox-contents">
<h3>Below is an overview of your scores</h3>
<div class="float-left">
<strong>Slowest:</strong><br />
<strong>Fastest:</strong><br />
<strong>Average:</strong>
</div>
<div class="float-right">
<span id="slow">-</span><br />
<span id="fast">-</span><br />
<span id="avg">-</span>
</div>
<div class="clear" id="after">&nbsp;</div>
<p id="submit_placeholder">&nbsp;</p>
</div><!-- #scores -->
<div id="submit" class="fancybox-contents" style="width: 300px;">
</div><!-- #submit -->
<footer>
<div class="left">
<p>&copy; <?php echo date('Y'); ?> <?php echo fancybox_text('#about','Wes Dews'); ?>. All rights reserved. <em>Not affiliated with HumanBenchmark.com</em></p>
</div>
<div class="right">
<a href="http://validator.w3.org/check?uri=http%3A%2F%2Fninjareflexes.com%2F" target="_blank">
<img src="images/html5-icon.png" alt="HTML5 Validation" />
</a>
<div class="clear show-660">&nbsp;</div>
<a href="http://jigsaw.w3.org/css-validator/check/referer" target="_blank">
<img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss-blue" alt="Valid CSS!" />
</a>
<div class="clear show-660">&nbsp;</div>
<span id="cdSiteSeal1">
<script type="text/javascript" src="//tracedseals.starfieldtech.com/siteseal/get?scriptId=cdSiteSeal1&amp;cdSealType=Seal1&amp;sealId=55e4ye7y7mb7343e7cf7409ee7edbde97aqy7mb7355e4ye72b271ff0064f1759"></script>
</span>
</div>
</footer>
</div><!-- .allwrapper -->
</body>
</html>