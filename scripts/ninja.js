	var releaseCount=Number(0);
	var releaseTime;
	var delayTime=Number(0);
	var delayTimeInterval;
	var startedClick=false;
	var coolWords=['Wowee','Jeepers','Yippee','Whoa','Shazam','Sweet','Holy','Exciting'];
	var sadWords=['Aww','Shucks','Bummer','Dang','Ugh','Yikes','Nope'];
	var oopsWords=['Oops', 'Darnit', 'Fudge', 'Nuts', 'Ahh', 'Yuck'];
	var attempts=[];
	var average=Number(0);
	var fastest = Number(0);
	var slowest = Number(0);
	var submittedVar = Number(0);

	$.fn.extend({
	    disableSelection: function () {
	        this.each(function () {
	            this.onselectstart = function () {
	                return false;
	            };
	            this.unselectable = "on";
	            $(this).css('-moz-user-select', 'none');
	            $(this).css('-webkit-user-select', 'none');
	        });
	    }
	});


	function enc(eString) {
	    var encArray={ 'A': '341', 'B': '892', 'C': '921', 'D': '231', 'E': '108', 'F': '747', 'G': '883', 'H': '015', 'I': '301', 'J': '188', 'K': '930', 'L': '661', 'M': '578', 'N': '034', 'O': '122', 'P': '339', 'Q': '412', 'R': '409', 'S': '291', 'T': '818', 'U': '110', 'V': '093', 'W': '234', 'X': '889', 'Y': '004', 'Z': '121', '0': '1A3', '1': 'F12', '2': '99P', '3': 'BB8', '4': '6B7', '5': '55L', '6': '7EE', '7': 'I17', '8': '89Y', '9': '3U3' };
	    var theString='';
	    var encString=String(eString);
	    for (var i=0; i<=encString.length-1; i++) {
	        theString += encArray[encString.charAt(i)];
	    }
	    return theString;
	}
	function addOptions() {
	    var options=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
	    var buildOptions="";
	    for (var i = 0; i <= options.length - 1; i++) {
	        buildOptions+='<option value="'+options[i]+'">'+options[i]+'</option>'+"\n";
	    }
	    $('#name1').html(buildOptions);
	    $('#name2').html(buildOptions);
	    $('#name3').html(buildOptions);
	    nameDisplay();
	}
	function removeOptions() {
	    $('#name1').html('');
	    $('#name2').html('');
	    $('#name3').html('');
	}
	function checkDisplays()
	{
	    if (fastest==submittedVar||attempts.length==0)
	    {
	        $('#details').html('');
	        $('#submit_placeholder').html('');
	        removeOptions();
	    }
	    if (fastest<submittedVar||attempts.length>0)
	    {
	        $('#details').html('<a class="fancybox button-small" href="#scores">Leaderboards \'n Stuff</a>');
	        $('#submit_placeholder').html('<a class="button-small fancybox" id="precursor" href="#submit">Submit Score</a>');
	        $('#submit').html('<p style="text-decoration: underline;"><strong>Initials</strong></p><select name="name1" id="name1" onchange="nameDisplay()"></select><select name="name2" id="name2" onchange="nameDisplay()"></select><select name="name3" id="name3" onchange="nameDisplay()"></select><p id="name_disp"></p><p style="text-decoration: underline;"><strong>Fastest Score</strong></p><p id="scoreToSubmit">-</p><input type="hidden" name="s" id="s" value="" /><input type="hidden" name="n" id="n" value="" /><p><a href="#" class="button-small" id="go" onclick="submitScores()">Put it up!</a></p>');
	        $('#s').val(enc(fastest));
	        $('#scoreToSubmit').text(fastest);
	        addOptions();
	    }
	}
	function submitted()
	{
	    $('#details').html('');
	    $('#submit_placeholder').html('');
	    removeOptions();
	    $('#submit').html('<p style="text-decoration: underline;">Thanks for your submission!</p><p>Your score will be visible to the public. When you get a faster time, you can submit that!</p>');
	}
	function nameDisplay()
	{
	    $('#name_disp').text($('#name1').val() + $('#name2').val() + $('#name3').val());
	    $('#n').val(enc($('#name1').val() + $('#name2').val() + $('#name3').val()));
	}
	function submitScores()
	{
	    $.ajax({
	        type: "POST",
	        url: "core/translate.php",
	        data: { n: $('#n').val(), s: $('#s').val() }
	    }).done(function(){
	        submittedVar=fastest;
	        submitted();
	    });
	}
$(function(){
	function timeReached()
	{
		delayTimeInterval=setInterval(function(){delayTime+=10},10);
		$('#bubble-container .shift').html('Release!');
	}
	function stopIntervals()
	{
		if(releaseTime) clearTimeout(releaseTime);
		clearInterval(delayTimeInterval);
	}
	function recordTime() {
	    var overallReaction=delayTime+releaseCount;
	    var diff=overallReaction-releaseCount;
	    attempts[attempts.length]=Number(diff);
	    if (fastest>diff||fastest==0) fastest=diff;
	    if (slowest<diff) slowest=diff;
	    getAverage();
	    $('#slow').text(slowest);
	    $('#fast').text(fastest);
	    $('#avg').text(average);
	}
	function getAverage()
	{
	    var total=Number(0);
	    for(var i=0; i<=attempts.length-1; i++)
	    {
	        total+=attempts[i];
	    }
	    var initial=total/attempts.length;
	    average=initial.toFixed();
	}
	function compareTimes()
	{
		if(delayTime>0)
		{
		    recordTime();
		    checkDisplays();
			var overallReaction=delayTime+releaseCount;
			var diff=overallReaction-releaseCount;
			if(diff<=200)
			{
				var min=0;
				var max=Number(coolWords.length) - 1;
				var rand=Number(Math.random()*(max-min)+min).toFixed();
				var coolWord=coolWords[rand];
				$('#bubble-container .shift').css('color','green');
				$('#bubble-container .shift').html(coolWord + '! You only went <strong>' + diff + 'ms</strong> over!');
			}
			else
			{
				var min=0;
				var max=Number(sadWords.length)-1;
				var rand=Number(Math.random()*(max-min)+min).toFixed();
				var sadWord=sadWords[rand];
				$('#bubble-container .shift').css('color','red');
				$('#bubble-container .shift').html(sadWord + '...no ninja. It took you <strong>'+diff+'ms</strong>.');
			}
		}
		else
		{
			var min=0;
			var max=Number(oopsWords.length)-1;
			var rand=Number(Math.random()*(max-min)+min).toFixed();
			var oopsWord=oopsWords[rand];
			$('#bubble-container .shift').css('color','orange');
			$('#bubble-container .shift').html(oopsWord + '! You got too eager. Easy, tiger...');
		}
	}
	function clearVars()
	{
		releaseCount=Number(0);
		releaseTime=null;
		delayTime=Number(0);
		delayTimeInterval=null;
		startedClick=false;
	}
	function resetBubble()
	{
		$('#bubble-container .shift').html('Oops! Make sure you keep your mouse on the button.');
	}
	$('#press').disableSelection();
	$('#press').mousedown(function (e) {
	    e.preventDefault();
		clearVars();
		startedClick=true;
		var goal=Math.random()*(35-10)+10; // (max-min)+min
		goal*=100;
		releaseCount=Number(goal.toFixed());
		releaseTime=setTimeout(timeReached,releaseCount);
		$('#bubble-container .shift').css('color','#555');
		$('#bubble-container .shift').html('Wait for it...');
	});
	$('#press').mouseup(function(){
		stopIntervals();
		compareTimes();
		clearVars();
	});
	$('#press').mouseout(function(){
		if(startedClick)
		{
		    stopIntervals();
			clearVars();
			resetBubble();
		}
	});
	$('.fancybox').fancybox({
	    'scrolling': 'no'
	});
});
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40861339-1', 'ninjareflexes.com');
ga('send', 'pageview');