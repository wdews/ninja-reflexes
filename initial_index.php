<div class="pad-left">
<div class="logo">
<h1>Ninja<br />Reflexes</h1>
<img id="logo-image" src="images/ninja.png" alt="Ninja Character" />
<div class="clear">&nbsp;</div>
</div>
<article id="before">
<p>Want to be a Ninja? You need to be speedy. Super-speedy. Click on the button and let go when you are told to! Do you
have what it takes to be a Ninja?</p>
<p>Based on findings at
<a href="http://www.humanbenchmark.com/tests/reactiontime/stats.php" title="Open in new window" target="_blank">HumanBenchmark.com</a>,
the average human reaction time is 215ms. Ninjas? They think that's slow. Slow = Bad.</p>
<ul>
<li><a href="/leaderboards">Leaderboards!</a></li>
</ul>
<div class="fb-like" data-href="http://ninjareflexes.com" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</article>
</div>