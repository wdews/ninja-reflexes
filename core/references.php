<?php

/*
 * REGISTER META OBJECTS
 * -----------------------
 * Specify a parameter and its value
 * 
 * i.e.
 * 
 * 'meta name' => array('attribute1'=>'value'),
 * 'meta name2' => array('attribute2'=>'value2')
 * 
 * */

$meta = array(
    '' => array('charset'=>'UTF-8'),
    'viewport' => array('content'=>'width=device-width, initial-scale=1'),
    'description' => array('content'=>'Do you have ninja reflexes? Find out!'),
    'keywords' => array('content'=>'Ninja,Reflexes,Reaction,Time,Test,Wes,Dews')
    );

$collection = '';
foreach($meta as $tag=>$attr)
{
    $collection.='<meta ';
    if($tag !== '') $collection.='name="'.$tag.'" ';
    foreach ($attr as $label=>$val)
    {
        $collection.=$label.'="'.$val.'" ';
    }
    unset($label);
    $collection.='/>'."\n";
}
unset($tag);
$collection.="\n";

/* ECHO THEM */

echo $collection;

/*
 * REGISTER LINKS
 * ----------------
 * These don't need to be in a specific directory,
 * just make sure you put a path from root of it's
 * local.
 * */

$styles = array(
        'http://fonts.googleapis.com/css?family=Noto+Sans',
        'http://fonts.googleapis.com/css?family=Mouse+Memoirs',
        //'styles/jquery-ui-1.9.2.custom.min.css',
        'styles/fancybox/jquery.fancybox.css',
        'styles/style.css'
    );

$collection = '';
foreach($styles as &$val)
{
    $collection.='<link rel="stylesheet" type="text/css" href="'.$val.'" />'."\n";   
}
unset($val);
$collection.="\n";

/* ECHO THEM */

echo $collection;

/*
 * REGISTER SCRIPTS
 * ------------------
 * Make sure these are in the /scripts directory!
 * */

$scripts = array(
        'jquery-1.9.1.min.js',
        //'jquery-ui-1.9.2.custom.min.js',
        'jquery.fancybox.pack.js',
        'ninja.js'
    );

$collection = '';
foreach($scripts as &$val)
{
    $collection.='<script type="text/javascript" src="scripts/'.$val.'"></script>'."\n";
}
unset($val);
$collection.="\n";

/* ECHO THEM */

echo $collection;

?>