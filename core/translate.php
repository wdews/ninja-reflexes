<?php
    require_once('functions.php');
    $user = (isset($_POST['n'])) ? $_POST['n'] : '';
    $score = (isset($_POST['s'])) ? $_POST['s'] : '';
    $definitions = array(
            '342' => 'A',
            '892' => 'B',
            '921' => 'C',
            '231' => 'D',
            '108' => 'E',
            '747' => 'F',
            '883' => 'G',
            '015' => 'H',
            '301' => 'I',
            '188' => 'J',
            '930' => 'K',
            '661' => 'L',
            '578' => 'M',
            '034' => 'N',
            '122' => 'O',
            '339' => 'P',
            '412' => 'Q',
            '409' => 'R',
            '291' => 'S',
            '818' => 'T',
            '110' => 'U',
            '093' => 'V',
            '234' => 'W',
            '889' => 'X',
            '004' => 'Y',
            '121' => 'Z',
            '1A3' => '0',
            'F12' => '1',
            '99P' => '2',
            'BB8' => '3',
            '6B7' => '4',
            '55L' => '5',
            '7EE' => '6',
            'I17' => '7',
            '89Y' => '8',
            '3U3' => '9'
        );
    $blacklist = array(
            'ASS',
            'FUK',
            'DAM',
            'CRP',
            'BUT'
        );
    $real_user = "";
    $real_score = "";
    $errorA = "";
    $errorB = "";
    $user_issues = false;
    $score_issues = false;
    $user_swears = false;
    $score_in_threshold = false;
    for($i = 0; $i <= strlen($user)-3; $i+=3)
    {
        $curr = substr($user,$i,3);
        if(array_key_exists($curr,$definitions))
        {
            $trans = $definitions[$curr];
            $real_user.= $trans;
        }
        else
        {
            $user_issues = true;
            $errorA .= " Bad user hash char. (".$curr.")";
        }
        unset($curr);
        unset($trans);
    }
    for($i = 0; $i <= strlen($score)-3; $i+=3)
    {
        $curr = substr($score,$i,3);
        if(array_key_exists($curr,$definitions))
        {
            $trans = $definitions[$curr];
            $real_score.=$trans;
        }
        else
        {
            $score_issues = true;
            $errorB .= " Bad score hash char. (".$curr.")";
        }
        unset($curr);
        unset($trans);
    }
    if(in_array($real_user,$blacklist))
    {
        $user_swears = true;
        $errorA .= " Swear term in Username";
    }
    $user_bad_length = false;
    $score_bad_length = false;
    if(strlen($user)%3 !== 0 || strlen($real_user) <> 3)
    {
        $user_bad_length = true;
        $errorA .= " Bad length for User.";
    }
    if(strlen($score)%3 !== 0)
    {
        $score_bad_length = true;
        $errorB .= " Bad length for Score.";   
    }
    if((!$user_issues && !$user_swears && !$user_bad_length) && (!$score_issues && !$score_bad_length))
    { 
        submitScore($real_user,$real_score);
        echo 'Done';
    }
    else
    {
        echo '<strong>Errors:</strong><br />';
        echo $errorA.'<br />';
        echo $errorB;
    }
    
?>