<?php

/*
 * FUNCTIONS
 * -----------
 * This file contains the functions library for
 * Ninja Reflexes. Keep it organized!
 * */

require_once('connect.php');

function fancybox_text($target,$contents)
{
    if(isset($target) && !empty($target) && isset($contents) && !empty($contents))
    {
        $object = '<a class="fancybox" href="'.$target.'">'.$contents.'</a>'."\n";
    }
    else $object = 'ERROR - Need both arguments specified, Target and Contents.';
    return $object;
}
function submitScore($user,$score)
{
    $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
        or die("Error connecting to database.");
    $user = mysqli_real_escape_string($dbc, trim($user));
    $score = mysqli_real_escape_string($dbc, trim($score));
    $query = "INSERT INTO leaderboard (name, score) VALUES ('$user','$score')";
    mysqli_query($dbc, $query)
        or die("Error querying database.");
    return "Score saved.";
}
function loadLeaders()
{
    $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
        or die("Error connecting to database.");
    $query = "SELECT * FROM leaderboard ORDER BY score ASC, time DESC LIMIT 10";
    $data = mysqli_query($dbc, $query)
        or die("Error querying database.");
    $table;
    $i = 1;
    while($row = mysqli_fetch_array($data))
    {
        $name = $row['name'];
        $score = $row['score'];
        $datetime = strtotime($row['time']);
        $datetime = date("Y-m-d H:i:s", $datetime);
        $table .= '<tr class="leaders leader-'.$i;
        if($i % 2 == 0) $table .= ' even';
        $table .= '"><td class="name"><div class="padded">'.$name.'</div></td><td class="score"><div class="padded">'.$score.'ms</div></td></tr>';
           $i++;
    }
    return $table;
}

?>